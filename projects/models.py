from django.db import models
from django.conf import settings

class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    host = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="host_projects",
        on_delete=models.PROTECT,
        null=True,
    )
    members = models.ManyToManyField(
        settings.AUTH_USER_MODEL, related_name="members_projects"
    )

    def __str__(self):
        return self.name

class Message(models.Model):
  user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
  project = models.ForeignKey('Project', on_delete=models.CASCADE)
  body = models.TextField()
  updated = models.DateTimeField(auto_now=True)
  created = models.DateTimeField(auto_now_add=True)

  def __str__(self):
    return self.body[0:50]