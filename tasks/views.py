from django.shortcuts import redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView
from tasks.models import Task
from .forms import TaskForm

# Create your views here.


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    form_class = TaskForm
    template_name = "tasks/taskcreate.html"

    def form_valid(self, form):
        new = form.save(commit=False)
        new.save()
        return redirect("show_project", pk=new.project.id)


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    context_object_name = "tasks"
    template_name = "tasks/tasklist.html"

    def get_queryset(self):
        user = self.request.user
        time = self.request.GET.get("time")
        if time == "urgent":
            return Task.objects.filter(assignee=user).order_by("due_date")
        if time == "not_urgent":
            return Task.objects.filter(assignee=user).order_by("-due_date")
        if time == "assigned":
            return Task.objects.filter(assignee=user)
        return Task.objects.filter(assignee=user)


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    fields = ["is_completed"]
    success_url = reverse_lazy("show_my_tasks")
